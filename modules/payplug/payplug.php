<?php
/**
 * 2013 - 2021 PayPlug SAS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send an email
 * to contact@payplug.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PayPlug module to newer
 * versions in the future.
 *
 * @author    PayPlug SAS
 * @copyright 2013 - 2021 PayPlug SAS
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PayPlug SAS
 */

/**
 * Check if prestashop Context
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_ . 'payplug/vendor/autoload.php');

class Payplug extends PaymentModule
{
    public $payplug_dependencies;

    /**
     * Constructor
     *
     * @return void
     * @throws Exception
     */
    public function __construct()
    {
        $this->name = 'payplug';
        $this->author = 'PayPlug';
        $this->bootstrap = true;
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->description = $this->l('payplug.construct.description');
        $this->displayName = 'PayPlug';
        $this->module_key = '1ee28a8fb5e555e274bd8c2e1c45e31a';
        $this->need_instance = true;
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => '1.8'];
        $this->tab = 'payments_gateways';
        $this->version = '3.5.0';

        parent::__construct();

        $this->module = false;

        if ($this->isValidPHPVersion()) {
            $this->setDependencies();
            $this->setModule();
        }
    }

    /**
     * @param bool $force_all
     * @return bool
     * @see Module::disable()
     *
     */
    public function disable($force_all = false)
    {
        if ($this->module) {
            return $this->module->disable($force_all);
        }
    }

    /**
     * @return string
     * @see Module::getContent()
     */
    public function getContent()
    {
        if ($this->module) {
            if (!$this->isValidInstallation()) {
                $this->install(true);
            }

            return (new \PayPlug\classes\AdminClass())->getContent();
        } else {
            $iso_code = Context::getContext()->language->iso_code;
            if ($iso_code == 'en' || $iso_code == 'gb') {
                $iso_code = 'en-gb';
            }
            $faq_url = 'https://support.payplug.com/hc/' . $iso_code.'/articles/360021267140';
            $this->context->smarty->assign('faq_url', $faq_url);

            $logo_url = __PS_BASE_URI__ . 'modules/payplug/views/img/logo_payplug.png';
            $this->context->smarty->assign('url_logo', $logo_url);

            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/payplug/views/css/admin-v3.5.0.css');

            return $this->display(__FILE__, '/views/templates/admin/php_version.tpl');
        }
    }

    /**
     * @description Get the module hook list from current Prestashop version
     */
    private function getHookList()
    {
        return [
            'actionAdminControllerSetMedia',
            'actionAdminPerformanceControllerAfter',
            'actionCarrierUpdate',
            'actionClearCompileCache',
            'actionDeleteGDPRCustomer',
            'actionExportGDPRData',
            'actionObjectCarrierAddAfter',
            'actionOrderStatusUpdate',
            'actionObjectOrderStateAddAfter',
            'actionObjectOrderStateUpdateAfter',
            'actionObjectOrderStateDeleteAfter',
            'actionUpdateLangAfter',
            'adminOrder',
            'customerAccount',
            'displayAdminOrderMain',
            'displayBackOfficeFooter',
            'displayBeforeShoppingCartBlock',
            'displayExpressCheckout',
            'displayProductPriceBlock',
            'displayAdminStatusesForm',
            'header',
            'moduleRoutes',
            'payment',
            'paymentReturn',
            'paymentOptions',
            'registerGDPRConsent',
        ];
    }

    /**
     * @description To load admin and admin_order (js and css) in order details in PS 1.7.7.0
     */
    public function hookActionAdminControllerSetMedia()
    {
        if ($this->module) {
            return $this->payplug_dependencies->getDependency('hook')->exe('actionAdminControllerSetMedia');
        }
    }

    /**
     * @description Flush PayPlugCache (PS 1.6), when PrestaShop cache cleared
     * @param $params
     * @return mixed
     */
    public function hookActionAdminPerformanceControllerAfter($params)
    {
        //todo: Rajouter le test de la table payplug cache avant d'executer ce code*/
        if ($this->module) {
            return $this->module->hookActionAdminPerformanceControllerAfter($params);
        }
    }

    /**
     * @description Flush PayPlugCache (PS 1.7), when PrestaShop cache cleared
     * @param $params
     * @return mixed
     */
    public function hookActionClearCompileCache($params)
    {
        //todo: Rajouter le test de la table payplug cache avant d'executer ce code
        if ($this->module) {
            return $this->module->hookActionClearCompileCache($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookActionDeleteGDPRCustomer($params)
    {
        if ($this->module) {
            return $this->module->hookActionDeleteGDPRCustomer($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookActionExportGDPRData($params)
    {
        if ($this->module) {
            return $this->module->hookActionExportGDPRData($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookActionOrderStatusUpdate($params)
    {
        if ($this->module) {
            return $this->module->hookActionOrderStatusUpdate($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookActionObjectOrderStateAddAfter($params)
    {
        if ($this->module) {
            return $this->payplug_dependencies->getDependency('hook')->exe('actionObjectOrderStateAddAfter', $params);
        }
    }

    public function hookActionObjectOrderStateUpdateAfter($params)
    {
        if ($this->module) {
            return $this->payplug_dependencies->getDependency('hook')->exe(
                'actionObjectOrderStateUpdateAfter',
                $params
            );
        }
    }
    public function hookActionObjectOrderStateDeleteAfter($params)
    {
        if ($this->module) {
            return $this->payplug_dependencies->getDependency('hook')->exe(
                'actionObjectOrderStateDeleteAfter',
                $params
            );
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookActionUpdateLangAfter($params)
    {
        if ($this->module) {
            return $this->module->hookActionUpdateLangAfter($params);
        }
    }

    /**
     * @description retrocompatibility of hookDisplayAdminOrderMain for version before 1.7.7.0
     * @param $params
     * @return mixed
     */
    public function hookAdminOrder($params)
    {
        if ($this->module) {
            return $this->module->hookAdminOrder($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookCustomerAccount($params)
    {
        if ($this->module) {
            return $this->module->hookCustomerAccount($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayAdminOrderMain($params)
    {
        if ($this->module) {
            return $this->module->hookDisplayAdminOrderMain($params);
        }
    }
    /**
     * @return mixed
     */
    public function hookDisplayAdminStatusesForm()
    {
        if ($this->module) {
            return $this->payplug_dependencies->getDependency('hook')->exe('displayAdminStatusesForm');
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayBackOfficeFooter($params)
    {
        if ($this->module) {
            return $this->module->hookDisplayBackOfficeFooter($params);
        }
    }

    /**
     * @description Display Oney CTA on Shopping cart page
     * @param $params
     * @return mixed
     */
    public function hookDisplayBeforeShoppingCartBlock($params)
    {
        if ($this->module) {
            return $this->module->hookDisplayBeforeShoppingCartBlock($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayExpressCheckout($params)
    {
        if ($this->module) {
            return $this->module->hookDisplayExpressCheckout($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookDisplayProductPriceBlock($params)
    {
        if ($this->module) {
            return $this->module->hookDisplayProductPriceBlock($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookHeader($params)
    {
        if ($this->module) {
            return $this->module->hookHeader($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     *
     * This hook is not used anymore in PS 1.7 but we have to keep it for retro-compatibility
     */
    public function hookPayment($params)
    {
        if ($this->module) {
            return $this->module->hookPayment($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookPaymentOptions($params)
    {
        if ($this->module) {
            return $this->module->hookPaymentOptions($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookPaymentReturn($params)
    {
        if ($this->module) {
            return $this->module->hookPaymentReturn($params);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    public function hookRegisterGDPRConsent($params)
    {
        if ($this->module) {
            return $this->module->hookRegisterGDPRConsent($params);
        }
    }

    /**
     * @description Install plugin
     * @param bool $soft_install
     * @return bool
     * @see Module::install()
     */
    public function install($soft_install = false)
    {
        if ($this->module) {
            $flag = true;

            // Use for update module is not fully installed
            if (!$soft_install) {
                $this->payplug_dependencies = null;
                $flag = $flag && parent::install();
                $this->setDependencies();
            }

            // Install configuration
            if ($flag) {
                $flag = $flag && $this->payplug_dependencies->getDependency('install')->install();
            }

            // Install hook
            if ($flag) {
                $hook_list = $this->getHookList();
                foreach ($hook_list as $hook) {
                    $flag = $flag && $this->registerHook($hook);
                }
            }

            return $flag;
        }

        return parent::install();
    }

    /**
     * @description Check if mobile is validated installation
     * @return bool
     */
    public function isValidInstallation()
    {
        if (Validate::isLoadedObject($this)) {
            return Configuration::hasKey('PAYPLUG_COMPANY_ID');
        }
        return true;
    }

    /**
     * @description test if php requiremnt is valid
     * @return array
     */
    public function isValidPHPVersion()
    {
        $php_min_version = 50600;

        if (!defined('PHP_VERSION_ID')) {
            $php_version = explode('.', PHP_VERSION);
            define('PHP_VERSION_ID', ($php_version[0] * 10000 + $php_version[1] * 100 + $php_version[2]));
        }

        return PHP_VERSION_ID >= $php_min_version;
    }

    /**
     * Run update module
     */
    public function runUpgradeModule()
    {
        if ($this->module) {
            $this->payplug_dependencies->getDependency('install')->checkOrderStates();
        }

        return parent::runUpgradeModule();
    }

    public function setDependencies()
    {
        $this->payplug_dependencies = new \PayPlug\classes\PayPlugDependencies();
    }

    private function setModule()
    {
        $this->module = $this->payplug_dependencies->payplug;
    }

    /**
     * @description Uninstall plugin
     * @return bool|mixed
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        if ($this->module) {
            return parent::uninstall() && $this->payplug_dependencies->getDependency('install')->uninstall();
        }

        return parent::uninstall();
    }
}
