<?php
/**
 * 2013 - 2021 PayPlug SAS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0).
 * It is available through the world-wide-web at this URL:
 * https://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web, please send an email
 * to contact@payplug.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PayPlug module to newer
 * versions in the future.
 *
 * @author    PayPlug SAS
 * @copyright 2013 - 2021 PayPlug SAS
 * @license   https://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PayPlug SAS
 */

namespace PayPlug\src\repositories;

class HookRepository extends Repository
{
    protected $constant;
    protected $payplug;
    protected $context;
    protected $tools;

    public function __construct($payplug, $constant, $context, $tools)
    {
        $this->payplug = $payplug;
        $this->constant = $constant;
        $this->context = $context;
        $this->tools = $tools;
    }

    public function actionAdminControllerSetMedia()
    {
        $module_url = $this->constant->get('__PS_BASE_URI__') . 'modules/payplug/';

        if ($this->context->getContext()->controller->controller_name == 'AdminOrders') {
            $this->payplug->mediaClass->setMedia([
                $module_url . 'views/css/admin_order-v3.5.0.css',
                $module_url . 'views/js/admin_order-v3.5.0.js',
                $module_url . 'views/js/utilities-v3.5.0.js',
            ]);
        } else {
            $this->payplug->mediaClass->setMedia([
                $module_url . 'views/js/admin-v3.5.0.js',
                $module_url . 'views/css/admin-v3.5.0.css',
            ]);
        }
    }

    /**
     * @description This is a hook function that allows
     * creating a new type of the order state
     * @param $param
     */
    public function actionObjectOrderStateAddAfter($param)
    {
        $order_state = $param['object'];
        $type = $this->tools->tool('getValue', 'order_state_type');
        return $this->payplug->getPlugin()->getOrderState()->saveType((int)$order_state->id, $type);
    }

    /**
     * @description This is a hook function that allows
     * to update the type of the order state
     * @param $param
     */
    public function actionObjectOrderStateUpdateAfter($param)
    {
        $order_state = $param['object'];
        if (isset($order_state->deleted) && $order_state->deleted) {
            return $this->actionObjectOrderStateDeleteAfter($param);
        }
        return $this->actionObjectOrderStateAddAfter($param);
    }

    /**
     * @description This is a hook function that deletes
     * an order state
     * @param $param
     */
    public function actionObjectOrderStateDeleteAfter($param)
    {
        $order_state = $param['object'];
        return $this->payplug->getPlugin()->getOrderState()->deleteType((int)$order_state->id);
    }

    /**
     * @description This hook is used to display
     * a select box in the order state page (BO)
     * in order to create/update a type
     * @param $param
     * @return mixed
     */
    public function displayAdminStatusesForm()
    {
        $types = [
            'undefined' => $this->l('hook.displayAdminStatusesForm.undefined'),
            'nothing' => $this->l('hook.displayAdminStatusesForm.orderStateTypeNothing'),
            'cancelled' => $this->l('hook.displayAdminStatusesForm.orderStateTypeCancelled'),
            'error' => $this->l('hook.displayAdminStatusesForm.orderStateTypeError'),
            'expired' => $this->l('hook.displayAdminStatusesForm.orderStateTypeExpired'),
            'paid' => $this->l('hook.displayAdminStatusesForm.orderStateTypePaid'),
            'pending' => $this->l('hook.displayAdminStatusesForm.orderStateTypePending'),
            'refund' => $this->l('hook.displayAdminStatusesForm.orderStateTypeRefund'),
        ];

        $id_order_state = $this->tools->tool('getValue', 'id_order_state');
        $current_order_state_type = $this->payplug->getPlugin()->getOrderState()->getType((int)$id_order_state);
        $payplug_order_state_url = 'https://support.payplug.com/hc/'
            . $this->payplug->context->language->iso_code
            . '/articles/4406805105298';
        $this->context->getContext()->smarty->assign([
            'payplug_order_state_url' => $payplug_order_state_url,
            'current_order_state_type' => $current_order_state_type,
            'order_state_types' => $types
        ]);

        return $this->payplug->fetchTemplate('order_state/type.tpl');
    }

    public function exe($method = false, $params = [])
    {
        if (!$method
            || !is_string($method)
            || !is_array($params)
            || !method_exists($this, $method)) {
            return false;
        }

        return $this->$method($params);
    }
}
