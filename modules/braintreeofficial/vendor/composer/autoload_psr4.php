<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'BraintreeofficialPPBTlib\\' => array($vendorDir . '/ppbtlib/src'),
    'Braintree\\' => array($vendorDir . '/braintree/braintree_php/lib/Braintree'),
    'BraintreeOfficialTest\\' => array($baseDir . '/202/tests'),
    'BraintreeOfficialAddons\\' => array($baseDir . '/'),
);
