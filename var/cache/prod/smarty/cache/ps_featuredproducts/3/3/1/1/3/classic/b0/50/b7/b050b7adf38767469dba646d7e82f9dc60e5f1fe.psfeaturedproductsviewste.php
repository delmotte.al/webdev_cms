<?php
/* Smarty version 3.1.39, created on 2021-12-21 22:26:23
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61c2467f56ec00_76777729',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1638886935,
      2 => 'module',
    ),
    '985fd1d5c467493d95da5f07fc48d945fd32e1c5' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\productlist.tpl',
      1 => 1638886935,
      2 => 'file',
    ),
    '83dd6d762f92abec44acc92522c8360f5dd88507' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1638886935,
      2 => 'file',
    ),
    'ca65dc4bb3793545c91c6eb761860cc05e494f0d' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\catalog\\_partials\\product-flags.tpl',
      1 => 1638886935,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_61c2467f56ec00_76777729 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Produits populaires
  </h2>
  

<div class="products row">
            
<div class="product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="25" data-id-product-attribute="0">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/prestashop/fr/accueil/25-pendentif-coeur.html" class="thumbnail product-thumbnail">
            <img
              class="img-fluid"
              src="http://localhost/prestashop/32-home_default/pendentif-coeur.jpg"
              alt="pendentif cœur"
              loading="lazy"
              data-full-size-image-url="http://localhost/prestashop/32-large_default/pendentif-coeur.jpg"
              width="250"
              height="250"
            />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/25-pendentif-coeur.html" content="http://localhost/prestashop/fr/accueil/25-pendentif-coeur.html">pendentif cœur</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Prix">
                                                  27,83 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="25" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag new">Nouveau produit</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div class="product col-xs-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="27" data-id-product-attribute="0">
    <div class="thumbnail-container">
      
                  <a href="http://localhost/prestashop/fr/accueil/27-montre-orient-sport-faa02002d-mako-ii.html" class="thumbnail product-thumbnail">
            <img
              class="img-fluid"
              src="http://localhost/prestashop/40-home_default/montre-orient-sport-faa02002d-mako-ii.jpg"
              alt="Montre Orient Sport..."
              loading="lazy"
              data-full-size-image-url="http://localhost/prestashop/40-large_default/montre-orient-sport-faa02002d-mako-ii.jpg"
              width="250"
              height="250"
            />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="http://localhost/prestashop/fr/accueil/27-montre-orient-sport-faa02002d-mako-ii.html" content="http://localhost/prestashop/fr/accueil/27-montre-orient-sport-faa02002d-mako-ii.html">Montre Orient Sport...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Prix de base">180,29 €</span>
                                  <span class="discount-percentage discount-product">-10%</span>
                              
              

              <span class="price" aria-label="Prix">
                                                  162,26 €
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="27" data-url="http://localhost/prestashop/fr/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag discount">-10%</li>
                    <li class="product-flag new">Nouveau produit</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

    </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://localhost/prestashop/fr/2-accueil">
    Tous les produits<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
