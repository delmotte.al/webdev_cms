<?php
/* Smarty version 3.1.39, created on 2021-12-21 22:11:15
  from 'C:\wamp64\www\prestashop\modules\payplug\views\templates\hook\customer\my_account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61c242f3624098_85550364',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd0306d8d29a90c23860d5af0e11376e665ac874d' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\modules\\payplug\\views\\templates\\hook\\customer\\my_account.tpl',
      1 => 1639748556,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61c242f3624098_85550364 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- MODULE Payplug -->
<?php if ($_smarty_tpl->tpl_vars['version']->value < 1.5) {?>
    <li>
        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_cards_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
            <img class="icon" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_icon_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
        </a>
        <a title="Bons de réduction" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_cards_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
</a>
    </li>
<?php } elseif ($_smarty_tpl->tpl_vars['version']->value < 1.6) {?>
    <li>
        <a title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_cards_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
            <img class="icon" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_icon_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
             <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>

        </a>
    </li>
<?php } elseif ($_smarty_tpl->tpl_vars['version']->value < 1.7) {?>
    <li>
        <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_cards_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
">
            <i class="icon-credit-card"></i>
            <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>
</span>
        </a>
    </li>
<?php } elseif ($_smarty_tpl->tpl_vars['version']->value < 1.8) {?>
    <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="savedcards-link" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['payplug_cards_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="material-icons">&#xE870;</i>
              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Saved cards','mod'=>'payplug'),$_smarty_tpl ) );?>

          </span>
    </a>
<?php } else { ?>
    <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your Prestashop version is not compatible','mod'=>'payplug'),$_smarty_tpl ) );?>
</p>
<?php }?>
<!-- END : MODULE Payplug -->
<?php }
}
